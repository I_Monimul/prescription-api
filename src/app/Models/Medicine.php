<?php

namespace Models;

class Medicine extends \Models\Base\Medicine
{
	public static function getAll()
	{
		$model = new self;
		$results = $model->find('deleted = 0');
		return empty($results) ? [] : $results->castAll();
	}

	public static function getOne($id)
	{
		$model = new self;
		$model->load(array('id = ? AND deleted = 0', $id));
		return empty($model) ? [] : $model->cast();
	}

	public static function post($data)
	{
		$medicine = explode(',', $data['medicine']);
		if ($medicine) {
			for ($i = 0; $i < count($medicine); $i++) {
				$model = new self;
				$model->medicine = strtoupper($medicine[$i]);
				$model->generics = strtoupper($data['generics']);
				$model->save();
			}
		}

		return empty($model) ? [] : $model->cast();
	}

	public static function put($id, $data)
	{
		$model = new self;
		$model->reset();
		$model->load(array('id = ? AND deleted = 0', $id));
		$model->medicine = strtoupper($data['medicine']);
		$model->generics = strtoupper($data['generics']);
		$model->save();
		return empty($model) ? [] : $model->cast();
	}

	public static function delete($id)
	{
		$model = new self;
		$model->load(array('id = ?', $id));
		$model->erase();
		return empty($model) ? [] : $model->cast();
	}

	public static function search($data)
	{
		$dataToSearch = $data['dataToSearch'];
		$limit = $data['limit'];
		$offset = $data['offset'];

		$model = new self;
		$query = '`' . $model->table . '`' . '.`deleted` <> 1' . ' AND (`medicine` LIKE "%' . $dataToSearch . '%" OR `generics` LIKE "%' . $dataToSearch . '%")';
		$results = $model->find($query, ['offset' => $offset, 'limit' => $limit, 'order' => 'medicine ASC']);

		return empty($results) ? [] : $results->castAll();
	}
}
