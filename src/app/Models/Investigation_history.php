<?php

namespace Models;

use \Models\Complaint as Complaint;
use \Models\Examination as Examination;
use \Models\Investigation as Investigation;
use \Models\Diagnosis as Diagnosis;
use \Models\Patient as Patient;
use \Models\Prescription_history as Prescription_history;

class Investigation_history extends \Models\Base\Investigation_history
{
	public static function getAll()
	{
		$model = new self;
		$results = $model->find('deleted = 0 order by id desc');

		if ($results) {
			for ($i = 0; $i < count($results); $i++) {
				$results[$i]->date = date('d/m/Y', strtotime($results[$i]->date));
			}
		}

		return empty($results) ? [] : $results->castAll();
	}

	public static function getOne($id)
	{
		$model = new self;
		$model->load(array('id = ? AND deleted = 0', $id));

		if ($model->complaints) {
			$complaints = explode(',', $model->complaints);
			for ($i = 0; $i < count($complaints); $i++) {
				$complaint = new Complaint;
				$result = $complaint->load(array('id = ? AND deleted = 0', $complaints[$i]));
				$result_complaint[$i] = $result->complaint;
			}
			$model->complaints = implode(',', $result_complaint);
		} else {
			$model->complaints = "";
		}

		if ($model->examinations) {
			$examinations = explode(',', $model->examinations);
			for ($i = 0; $i < count($examinations); $i++) {
				$examination = new Examination;
				$result = $examination->load(array('id = ? AND deleted = 0', $examinations[$i]));
				$result_examination[$i] = $result->examination;
			}
			$model->examinations = implode(',', $result_examination);
		} else {
			$model->examinations = "";
		}

		if ($model->investigations) {
			$investigations = explode(',', $model->investigations);
			for ($i = 0; $i < count($investigations); $i++) {
				$investigation = new Investigation;
				$result = $investigation->load(array('id = ? AND deleted = 0', $investigations[$i]));
				$result_investigation[$i] = $result->investigation;
			}
			$model->investigations = implode(',', $result_investigation);
		} else {
			$model->investigations = "";
		}

		if ($model->diagnoses) {
			$diagnoses = explode(',', $model->diagnoses);
			for ($i = 0; $i < count($diagnoses); $i++) {
				$diagnosis = new Diagnosis;
				$result = $diagnosis->load(array('id = ? AND deleted = 0', $diagnoses[$i]));
				$result_diagnosis[$i] = $result->diagnosis;
			}
			$model->diagnoses = implode(',', $result_diagnosis);
		} else {
			$model->diagnoses = "";
		}

		return empty($model) ? [] : $model->cast();
	}

	public static function post($data)
	{
		$model = new self;
		$model->patient = $data['patient']['id'];

		if ($data['complaints']) {
			$complaints = explode(',', $data['complaints']);
			for ($i = 0; $i < count($complaints); $i++) {
				if ($complaints[$i] && isset($complaints[$i])) {
					$complaint = new Complaint;
					$result = $complaint->load(array('complaint = ? AND deleted = 0', $complaints[$i]));
					if (!$result->dry()) {
						$result_complaint[$i] = $result->id;
					} else {
						$complaint->reset();
						$complaint->complaint = ucwords($complaints[$i]);
						$complaint->save();
						$result_complaint[$i] = $complaint->id;
					}
				} else {
					$result_complaint[$i] = null;
				}
			}
			$model->complaints = implode(',', $result_complaint);

			$model->complaint_descriptions = ucwords($data['complaint_descriptions']);
		}

		if ($data['examinations']) {
			$examinations = explode(',', $data['examinations']);
			for ($i = 0; $i < count($examinations); $i++) {
				if ($examinations[$i] && isset($examinations[$i])) {
					$examination = new Examination;
					$result = $examination->load(array('examination = ? AND deleted = 0', $examinations[$i]));
					if (!$result->dry()) {
						$result_examination[$i] = $result->id;
					} else {
						$examination->reset();
						$examination->examination = ucwords($examinations[$i]);
						$examination->save();
						$result_examination[$i] = $examination->id;
					}
				} else {
					$result_examination[$i] = null;
				}
			}
			$model->examinations = implode(',', $result_examination);

			$model->examination_descriptions = ucwords($data['examination_descriptions']);
		}

		if ($data['investigations']) {
			$investigations = explode(',', $data['investigations']);
			for ($i = 0; $i < count($investigations); $i++) {
				if ($investigations[$i] && isset($investigations[$i])) {
					$investigation = new Investigation;
					$result = $investigation->load(array('investigation = ? AND deleted = 0', $investigations[$i]));
					if (!$result->dry()) {
						$result_investigation[$i] = $result->id;
					} else {
						$investigation->reset();
						$investigation->investigation = ucwords($investigations[$i]);
						$investigation->save();
						$result_investigation[$i] = $investigation->id;
					}
				} else {
					$result_investigation[$i] = null;
				}
			}
			$model->investigations = implode(',', $result_investigation);
		}

		$model->date = date("Y-m-d");
		$model->save();
		return empty($model) ? [] : $model->cast();
	}

	public static function put($id, $data)
	{
		$model = new self;
		$model->reset();
		$model->load(array('id = ? AND deleted = 0', $id));

		if ($data['complaints']) {
			$complaints = explode(',', $data['complaints']);
			for ($i = 0; $i < count($complaints); $i++) {
				if ($complaints[$i] && isset($complaints[$i])) {
					$complaint = new Complaint;
					$result = $complaint->load(array('complaint = ? AND deleted = 0', $complaints[$i]));
					if (!$result->dry()) {
						$result_complaint[$i] = $result->id;
					} else {
						$complaint->reset();
						$complaint->complaint = ucwords($complaints[$i]);
						$complaint->save();
						$result_complaint[$i] = $complaint->id;
					}
				} else {
					$result_complaint[$i] = null;
				}
			}
			$model->complaints = implode(',', $result_complaint);

			$model->complaint_descriptions = ucwords($data['complaint_descriptions']);
		}

		if ($data['examinations']) {
			$examinations = explode(',', $data['examinations']);
			for ($i = 0; $i < count($examinations); $i++) {
				if ($examinations[$i] && isset($examinations[$i])) {
					$examination = new Examination;
					$result = $examination->load(array('examination = ? AND deleted = 0', $examinations[$i]));
					if (!$result->dry()) {
						$result_examination[$i] = $result->id;
					} else {
						$examination->reset();
						$examination->examination = ucwords($examinations[$i]);
						$examination->save();
						$result_examination[$i] = $examination->id;
					}
				} else {
					$result_examination[$i] = null;
				}
			}
			$model->examinations = implode(',', $result_examination);

			$model->examination_descriptions = ucwords($data['examination_descriptions']);
		}

		if ($data['investigations']) {
			$investigations = explode(',', $data['investigations']);
			for ($i = 0; $i < count($investigations); $i++) {
				if ($investigations[$i] && isset($investigations[$i])) {
					$investigation = new Investigation;
					$result = $investigation->load(array('investigation = ? AND deleted = 0', $investigations[$i]));
					if (!$result->dry()) {
						$result_investigation[$i] = $result->id;
					} else {
						$investigation->reset();
						$investigation->investigation = ucwords($investigations[$i]);
						$investigation->save();
						$result_investigation[$i] = $investigation->id;
					}
				} else {
					$result_investigation[$i] = null;
				}
			}
			$model->investigations = implode(',', $result_investigation);
		}

		$model->save();
		return empty($model) ? [] : $model->cast();
	}

	public static function delete($id)
	{
		$model = new self;
		$model->load(array('id = ?', $id));
		$model->erase();
		return empty($model) ? [] : $model->cast();
	}

	public static function search($data)
	{
		$model = new self;

		$fromDate = $data['fromDate'];
		$toDate = $data['toDate'];
		$query = '`' . $model->table . '`' . '.`deleted` <> 1' . ' AND `date` >= "' . $fromDate . '" AND `date` <= "' . $toDate . '"';

		$name = $data['patient'];
		$patient = new Patient;
		$patients = $patient->find('name LIKE "%' . $name . '%" AND deleted <> 1 order by id desc');

		if (!empty($patients)) {
			$query .= ' AND (';
			for ($i = 0; $i < count($patients); $i++) {
				if ($i > 0) {
					$query .= ' OR';
				}
				$query .= ' `patient` = "' . $patients[$i]->id . '"';
			}
			$query .= ')';
		}

		$results = $model->find($query, ['order' => 'id DESC']);
		if (empty($results)) {
			return [];
		} else {
			$_results = $results->castAll();

			$status = $data['status'];
			if ($status == 'P' && !empty($_results)) {
				for ($i = 0; $i < count($_results); $i++) {
					$prescription = new Prescription_history;
					$prescription_history = $prescription->load(array('investigation_history = ? AND deleted = 0 order by id desc', $results[$i]->id));
					if ($prescription_history->dry()) {
						unset($_results[$i]);
					}
				}
			} else if ($status == 'N' && !empty($_results)) {
				for ($i = 0; $i < count($_results); $i++) {
					$prescription = new Prescription_history;
					$prescription_history = $prescription->load(array('investigation_history = ? AND deleted = 0 order by id desc', $results[$i]->id));
					if (!$prescription_history->dry()) {
						unset($_results[$i]);
					}
				}
			} else {
			}
		}

		return array_values($_results);
	}
}
