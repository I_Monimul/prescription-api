<?php

namespace Models;

use \Models\Prescription_history as Prescription_history;
use \Models\Medicine as Medicine;
use \Models\Time as Time;
use \Models\Comment as Comment;
use \Models\Duration as Duration;
use \Models\Investigation_history as Investigation_history;
use \Models\Complaint as Complaint;
use \Models\Examination as Examination;
use \Models\Investigation as Investigation;
use \Models\Diagnosis as Diagnosis;
use \Models\Advice as Advice;
use \Models\Next_visit as Next_visit;
use \Models\Referee as Referee;

class Pdf
{
	public static function getOne($id)
	{
		$id = explode(',', $id);
		if ($id[0] == 'p') {
			//prs
			$model = new Prescription_history;
			$model->load(array('id = ? AND deleted = 0', $id[1]));

			$medicines_html = "";
			$medicines = explode(',', $model->medicines);
			$comments = explode(',', $model->comments);
			$times = explode(',', $model->times);
			$durations = explode(',', $model->durations);
			for ($i = 0; $i < count($medicines); $i++) {
				$medicine = new Medicine;
				$result = $medicine->load(array('id = ? AND deleted = 0', $medicines[$i]));
				$result_medicine = $result->medicine;
				$time = new Time;
				$result = $time->load(array('id = ? AND deleted = 0', $times[$i]));
				if ($result->alternative) {
					$result_time = $result->alternative;
				} else {
					$result_time = $result->time;
				}
				$comment = new Comment;
				$result = $comment->load(array('id = ? AND deleted = 0', $comments[$i]));
				if ($result->alternative) {
					$result_comment = $result->alternative;
				} else {
					$result_comment = $result->comment;
				}
				$duration = new Duration;
				$result = $duration->load(array('id = ? AND deleted = 0', $durations[$i]));
				if ($result->alternative) {
					$result_duration = $result->alternative;
				} else {
					$result_duration = $result->duration;
				}

				//$medicines_html .= '<tr><td style="padding-left: 20px;">' . $result_medicine . '</td><td>' . $result_time . '</td><td>' . $result_comment . '</td><td>' . $result_duration . '</td></tr>';
				$medicines_html .= '<tr><td style="padding-left: 20px; border-bottom: 1px solid #dbdbdb;">' . $result_medicine . '</td><td style="border-bottom: 1px solid #dbdbdb;">' . $result_time . '<br />' . $result_comment . '</td><td style="border-bottom: 1px solid #dbdbdb;">' . $result_duration . '</td></tr>';
			}
			//prs

			//oth
			$advices_html = '<tr style="font-size: 9pt;"><td>উপদেশঃ</td></tr>';
			if ($model->advices) {
				$advices = explode(',', $model->advices);
				for ($i = 0; $i < count($advices); $i++) {
					$advice = new Advice;
					$result = $advice->load(array('id = ? AND deleted = 0', $advices[$i]));
					if ($result->alternative) {
						$result_advice = $result->alternative;
					} else {
						$result_advice = $result->advice;
					}

					$sl = strval($i + 1);
					$sl_bl = '';
					for ($j = 0; $j < strlen($sl); $j++) {
						switch ($sl[$j]) {
							case '1':
								$sl_bl .= '১';
								break;
							case '2':
								$sl_bl .= '২';
								break;
							case '3':
								$sl_bl .= '৩';
								break;
							case '4':
								$sl_bl .= '৪';
								break;
							case '5':
								$sl_bl .= '৫';
								break;
							case '6':
								$sl_bl .= '৬';
								break;
							case '7':
								$sl_bl .= '৭';
								break;
							case '8':
								$sl_bl .= '৮';
								break;
							case '9':
								$sl_bl .= '৯';
								break;
							default:
								$sl_bl .= '০';
						}
					}

					$advices_html .= '<tr style="font-size: 10pt;"><td style="padding-left: 20px;">' . $sl_bl . '. ' . $result_advice . '</td></tr>';
				}
			} else {
				$advices_html = "";
			}

			if ($model->next_visit) {
				$next_visit = new Next_visit;
				$result = $next_visit->load(array('id = ? AND deleted = 0', $model->next_visit));
				if ($result->alternative) {
					$result_next_visit = $result->alternative;
				} else {
					$result_next_visit = $result->next_visit;
				}

				$next_visit_html = '<tr style="font-size: 10pt;"><td><br/><span>পরবর্তী সাক্ষাৎ: </span><span style="color: #ff0000;">' . $result_next_visit . '</span></td></tr>';
			} else {
				$next_visit_html = "";
			}

			if ($model->referee) {
				$referee = new Referee;
				$result = $referee->load(array('id = ? AND deleted = 0', $model->referee));
				$result_referee = $result->referee;

				$referee_html = '<tr style="font-size: 10pt;"><td><span>Refered to: </span>' . $result_referee . '</td></tr>';
			} else {
				$referee_html = "";
			}
			//oth

			//inv
			if ($model->investigation_history) {
				if ($model->investigation_history->complaints) {
					$complaints_html = "";
					$complaints = explode(',', $model->investigation_history->complaints);
					$complaint_descriptions = explode(',', $model->investigation_history->complaint_descriptions);
					for ($i = 0; $i < count($complaints); $i++) {
						$complaint = new Complaint;
						$result = $complaint->load(array('id = ? AND deleted = 0', $complaints[$i]));
						$complaints_html .= '<tr><td style="font-size: 9pt;"><i>' . $result->complaint . '</i>';
						if ($complaint_descriptions[$i]) {
							$complaints_html .= ': ' . $complaint_descriptions[$i];
						}
						$complaints_html .= '</td></tr>';
					}
				} else {
					// $complaints_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
					$complaints_html = null;
				}

				if ($model->investigation_history->examinations) {
					$examinations_html = "";
					$examinations = explode(',', $model->investigation_history->examinations);
					$examination_descriptions = explode(',', $model->investigation_history->examination_descriptions);
					for ($i = 0; $i < count($examinations); $i++) {
						$examination = new Examination;
						$result = $examination->load(array('id = ? AND deleted = 0', $examinations[$i]));
						$examinations_html .= '<tr><td style="font-size: 9pt;"><i>' . $result->examination . '</i>';
						if ($examination_descriptions[$i]) {
							$examinations_html .= ': ' . $examination_descriptions[$i];
						} else {
							$examinations_html .= ': NAD';
						}
						$examinations_html .= '</td></tr>';
					}
				} else {
					// $examinations_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
					$examinations_html = null;
				}

				if ($model->investigation_history->investigations) {
					$investigations_html = "";
					$investigations = explode(',', $model->investigation_history->investigations);
					$investigation_descriptions = explode(',', $model->investigation_history->investigation_descriptions);
					for ($i = 0; $i < count($investigations); $i++) {
						$investigation = new Investigation;
						$result = $investigation->load(array('id = ? AND deleted = 0', $investigations[$i]));
						$investigations_html .= '<tr><td style="font-size: 9pt;"><i>' . $result->investigation . '</i>';
						if ($investigation_descriptions[$i]) {
							$investigations_html .= ': ' . $investigation_descriptions[$i];
						}
						$investigations_html .= '</td></tr>';
					}
				} else {
					// $investigations_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
					$investigations_html = null;
				}

				if ($model->investigation_history->diagnoses) {
					$diagnoses_html = "";
					$diagnoses = explode(',', $model->investigation_history->diagnoses);
					$diagnosis_descriptions = explode(',', $model->investigation_history->diagnosis_descriptions);
					for ($i = 0; $i < count($diagnoses); $i++) {
						$diagnosis = new Diagnosis;
						$result = $diagnosis->load(array('id = ? AND deleted = 0', $diagnoses[$i]));
						$diagnoses_html .= '<tr><td style="font-size: 9pt;"><i>' . $result->diagnosis . '</i>';
						if ($diagnosis_descriptions[$i]) {
							$diagnoses_html .= ': ' . $diagnosis_descriptions[$i];
						}
						$diagnoses_html .= '</td></tr>';
					}
				} else {
					// $diagnoses_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
					$diagnoses_html = null;
				}
			} else if ($model->diagnoses) {
				$complaints_html = null;
				$examinations_html = null;
				$investigations_html = null;

				$diagnoses_html = "";
				$diagnoses = explode(',', $model->diagnoses);
				$diagnosis_descriptions = explode(',', $model->diagnosis_descriptions);
				for ($i = 0; $i < count($diagnoses); $i++) {
					$diagnosis = new Diagnosis;
					$result = $diagnosis->load(array('id = ? AND deleted = 0', $diagnoses[$i]));
					$diagnoses_html .= '<tr><td style="font-size: 9pt;"><i>' . $result->diagnosis . '</i>';
					if ($diagnosis_descriptions[$i]) {
						$diagnoses_html .= ': ' . $diagnosis_descriptions[$i];
					}
					$diagnoses_html .= '</td></tr>';
				}
			} else {
				// $complaints_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
				// $examinations_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
				// $investigations_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
				// $diagnoses_html = '<tr><td style="font-size: 9pt;">None</td></tr>';
				$complaints_html = null;
				$examinations_html = null;
				$investigations_html = null;
				$diagnoses_html = null;
			}
			//inv

			//ptn
			if ($model->patient->gender == 'M') {
				$patient_gender = "Male";
			} else if ($model->patient->gender == 'F') {
				$patient_gender = "Female";
			} else {
				$patient_gender = "Others";
			}
			//ptn

			$html = '
				<table width="700" style="padding-top:94px;">
					<tr>
						<td width="27%" style="padding-left: 5px;">&emsp;' . $model->patient->name . '</td>
						<td width="39%" style="padding-left: 5px;">ID: ' . sprintf("%06d", $id[1]) . '</td>
						<td width="20%" style="padding-left: 9px;">&nbsp;' . $model->patient->age . ' years</td>
						<td width="10%" style="padding-left: 10px;">' . date("d.m.Y", strtotime($model->date)) . '</td>
					</tr>
				</table>
				<br/>
				<br/>
				<table width="700">
					<tr>
						<td>
							<table width="225">
								<tr>
									<td style="padding-bottom: 20px;">
										<span style="font-size: 8pt;">' . sprintf("%06d", $model->id) . '</span>
										<br />
										<barcode code="P-' . $model->id . '" size="0.6" type="QR" error="M" class="barcode" disableborder="1" />
									</td>
								</tr>
								' . ($complaints_html ? '
								<tr>
									<td><strong style="font-size: 10pt;">Chief complaints</strong></td>
								</tr>
								' . $complaints_html . '<br/><br/> ' : '') . '
								' . ($examinations_html ? '<tr>
									<td><strong style="font-size: 10pt;">On examination</strong></td>
								</tr>
								' . $examinations_html . '<br/><br/> ' : '') . '
								' . ($investigations_html ? '<tr>
									<td><strong style="font-size: 10pt;">Investigations</strong></td>
								</tr>
								' . $investigations_html . '<br/><br/> ' : '') . '
								' . ($diagnoses_html ? '<tr>
									<td><strong style="font-size: 10pt;">Diagnoses</strong></td>
								</tr>
								' . $diagnoses_html . ' ' : '') . '
							</table>
						</td>
						<td style=" vertical-align: top;">
							<table width="475" style="font-family: solaimanlipi; font-size: 10pt; padding-left: 10px;">
								<tr>
									<td><img src="../public/images/Rx.png" alt="Rx" width="3%"></td>
								</tr>
								' . $medicines_html . '
							</table>
							<br/>
							<table width="500" style="font-family: solaimanlipi; font-size: 10pt; padding-left: 10px;">
								' . $advices_html . '
								' . $next_visit_html . '
								' . $referee_html . '
							</table>
						</td>
					</tr>
				</table>
			';
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
			$mpdf->WriteHTML($html);
			return $mpdf->Output();
		} else if ($id[0] == 's') {
			//prs
			$model = new Prescription_history;
			$model->load(array('id = ? AND deleted = 0', $id[1]));

			$medicines_html = "";
			$medicines = explode(',', $model->medicines);
			$comments = explode(',', $model->comments);
			$times = explode(',', $model->times);
			$durations = explode(',', $model->durations);
			for ($i = 0; $i < count($medicines); $i++) {
				$medicine = new Medicine;
				$result = $medicine->load(array('id = ? AND deleted = 0', $medicines[$i]));
				$result_medicine = $result->medicine;
				$time = new Time;
				$result = $time->load(array('id = ? AND deleted = 0', $times[$i]));
				if ($result->alternative) {
					$result_time = $result->alternative;
				} else {
					$result_time = $result->time;
				}
				$comment = new Comment;
				$result = $comment->load(array('id = ? AND deleted = 0', $comments[$i]));
				if ($result->alternative) {
					$result_comment = $result->alternative;
				} else {
					$result_comment = $result->comment;
				}
				$duration = new Duration;
				$result = $duration->load(array('id = ? AND deleted = 0', $durations[$i]));
				if ($result->alternative) {
					$result_duration = $result->alternative;
				} else {
					$result_duration = $result->duration;
				}

				$medicines_html .= '<tr><td style="padding-left: 20px; border-bottom: 1px solid #dbdbdb;">' . $result_medicine . '</td><td style="border-bottom: 1px solid #dbdbdb;">' . $result_time . '<br />' . $result_comment . '</td><td style="border-bottom: 1px solid #dbdbdb;">' . $result_duration . '</td></tr>';
			}
			//prs

			//oth
			$advices_html = '<tr style="font-size: 9pt;"><td>উপদেশঃ</td></tr>';
			if ($model->advices) {
				$advices = explode(',', $model->advices);
				for ($i = 0; $i < count($advices); $i++) {
					$advice = new Advice;
					$result = $advice->load(array('id = ? AND deleted = 0', $advices[$i]));
					if ($result->alternative) {
						$result_advice = $result->alternative;
					} else {
						$result_advice = $result->advice;
					}

					$sl = strval($i + 1);
					$sl_bl = '';
					for ($j = 0; $j < strlen($sl); $j++) {
						switch ($sl[$j]) {
							case '1':
								$sl_bl .= '১';
								break;
							case '2':
								$sl_bl .= '২';
								break;
							case '3':
								$sl_bl .= '৩';
								break;
							case '4':
								$sl_bl .= '৪';
								break;
							case '5':
								$sl_bl .= '৫';
								break;
							case '6':
								$sl_bl .= '৬';
								break;
							case '7':
								$sl_bl .= '৭';
								break;
							case '8':
								$sl_bl .= '৮';
								break;
							case '9':
								$sl_bl .= '৯';
								break;
							default:
								$sl_bl .= '০';
						}
					}

					$advices_html .= '<tr style="font-size: 10pt;"><td style="padding-left: 20px;">' . $sl_bl . '. ' . $result_advice . '</td></tr>';
				}
			} else {
				$advices_html = "";
			}

			if ($model->next_visit) {
				$next_visit = new Next_visit;
				$result = $next_visit->load(array('id = ? AND deleted = 0', $model->next_visit));
				if ($result->alternative) {
					$result_next_visit = $result->alternative;
				} else {
					$result_next_visit = $result->next_visit;
				}

				$next_visit_html = '<tr style="font-size: 10pt;"><td><br/><span>পরবর্তী সাক্ষাৎ: </span><span style="color: #ff0000;">' . $result_next_visit . '</span></td></tr>';
			} else {
				$next_visit_html = "";
			}

			if ($model->referee) {
				$referee = new Referee;
				$result = $referee->load(array('id = ? AND deleted = 0', $model->referee));
				$result_referee = $result->referee;

				$referee_html = '<tr style="font-size: 10pt;"><td><span>Refered to: </span>' . $result_referee . '</td></tr>';
			} else {
				$referee_html = "";
			}
			//oth

			//ptn
			if ($model->patient->gender == 'M') {
				$patient_gender = "Male";
			} else if ($model->patient->gender == 'F') {
				$patient_gender = "Female";
			} else {
				$patient_gender = "Others";
			}
			//ptn

			$html = '
				<table width="400" style="margin-left: 50%;">
					<tr>
						<td width="100" colspan="" style="padding-bottom: 20px;">
							<span style="font-size: 8pt;">' . sprintf("%06d", $model->id) . '</span>
							<br />
							<barcode code="P-' . $model->id . '" size="0.6" type="QR" error="M" class="barcode" disableborder="1" />
						</td>
						<td colspan="2" style="font-size: 10px; text-align: center; padding-bottom: 20px;">
						<h3 style="font-size: 11px;">
							Dr. Md. Ruhul Amin</h3><br/>
							MBBS, MD (Internal medicine)<br/>
							Consultant, Medicine<br/>
							Medicine specialist<br/>
							Rajshahi Medical College Hospital<br/>
							Rajshahi
						</td>
					</tr>
					<tr>
						<td style="font-size: 11px;">ID: <b>' . sprintf("%06d", $id[1]) . '</b></td>
						<td style="font-size: 11px;">Date: <b>' . date("d.m.Y", strtotime($model->date)) . '</b></td>
					</tr>
					<tr>
						<td style="font-size: 11px; padding: 10px 0px 20px 0px;">Name: <b>' . $model->patient->name . '</b></td>
						<td style="font-size: 11px; padding: 10px 0px 20px 0px;">Age: <b>' . $model->patient->age . ' years</b></td>
					</tr>
				</table>
				<table width="400" style="font-family: solaimanlipi; font-size: 8pt; margin-left: 50%;">
					<tr>
						<td><img src="../public/images/Rx.png" alt="Rx" width="3%"></td>
					</tr>
					' . $medicines_html . '
				</table>
				<br/>
				<table width="400" style="font-family: solaimanlipi; font-size: 10pt; margin-left: 50%;">
					' . $advices_html . '
					' . $next_visit_html . '
					' . $referee_html . '
				</table>
			';
			$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
			$mpdf->WriteHTML($html);
			return $mpdf->Output();
		} else if ($id[0] == 'i') {
			//inv
			$model = new Investigation_history;
			$model->load(array('id = ? AND deleted = 0', $id[1]));

			if ($model->investigations) {
				$investigations_html = "";
				$investigations = explode(',', $model->investigations);
				for ($i = 0; $i < count($investigations); $i++) {
					$investigation = new Investigation;
					$result = $investigation->load(array('id = ? AND deleted = 0', $investigations[$i]));
					$investigations_html .= '<tr><td style="padding-left: 20px; font-size: 10px;">' . ($i + 1) . '. <i>' . $result->investigation . '</i></td></tr>';
				}
			} else {
				$investigations_html = '<tr><td style="padding-left: 20px;">None</td></tr>';
			}
			//inv

			//ptn
			if ($model->patient->gender == 'M') {
				$patient_gender = "Male";
			} else if ($model->patient->gender == 'F') {
				$patient_gender = "Female";
			} else {
				$patient_gender = "Others";
			}
			//ptn

			$html = '
				<table width="400" style="margin-left: 50%;">
					<tr>
						<td width="150" style="padding-top: 40px;">
							<span style="font-size: 8pt;">' . sprintf("%06d", $model->id) . '</span>
							<br />
							<barcode code="I-' . $model->id . '" size="0.6" type="QR" error="M" class="barcode" disableborder="1" />
						</td>
					</tr>
					<tr>
						<td style="font-size: 11px;">ID: <b>' . sprintf("%06d", $id[1]) . '</b></td>
						<td style="font-size: 11px; padding-left: 120px;">Date: <b>' . date("d.m.Y", strtotime($model->date)) . '</b></td>
					</tr>
					<tr>
						<td style="font-size: 11px; padding: 10px 0px 20px 0px;">Name: <b>' . $model->patient->name . '</b></td>
						<td style="font-size: 11px; padding: 10px 0px 20px 120px;">Age: <b>' . $model->patient->age . ' years</b></td>
					</tr>
				</table>
				<table width="400" style="margin-left: 50%;">
					<tr>
						<td style="font-size: 8pt;"><h3>Investigations</h3></td>
					</tr>
					' . $investigations_html . '
					<tr>
						<td style="padding-top: 40px; font-size: 10px; text-align: center;">
						<h3 style="font-size: 11px;">
							Dr. Md. Ruhul Amin</h3><br/>
							MBBS, MD (Internal medicine)<br/>
							Consultant, Medicine<br/>
							Medicine specialist<br/>
							Rajshahi Medical College Hospital<br/>
							Rajshahi
						</td>
					</tr>
				</table>
			';
			$mpdf = new \Mpdf\Mpdf(['mode' => 'UTF-8', 'format' => 'A4-L']);
			$mpdf->WriteHTML($html);
			return $mpdf->Output();
		}
	}
}
